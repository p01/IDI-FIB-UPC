#include "MyQLCDNumber.h"

MyQLCDNumber::MyQLCDNumber(QWidget *parent) : QLCDNumber(parent)
{
  op1 = 0;
  op2 = 0;
  resultat = 0;
  absolut = false;
}

void MyQLCDNumber::reset()
{
    op1 = 0;
    op2 = 0;
    resultat = 0;
    display(0);
}

void MyQLCDNumber::rebreop1(int numero)
{
    op1 = numero;
}

void MyQLCDNumber::rebreop2(int numero)
{
    op2 = numero;
}

void MyQLCDNumber::suma()
{
  resultat = op1 + op2;
  display(resultat);
}

void MyQLCDNumber::resta()
{
  resultat = op1 - op2;
  if(resultat < 0 and absolut) resultat = resultat * -1;
  display(resultat);
}

void MyQLCDNumber::multiplica()
{
  resultat = op1 * op2;
  display(resultat);
}

void MyQLCDNumber::divideix()
{
  resultat = op1/op2;
  display(resultat);
}

void MyQLCDNumber::absolut_activat()
{
  absolut = !absolut;
}

void MyQLCDNumber::acumula()
{
  op1 = resultat;
}
