#include <QApplication>
#include "Calculadora.h"

int main (int argc, char **argv)
{
  QApplication a( argc, argv);
  Calculadora calc;
  calc.show();
  return a.exec ();
}
