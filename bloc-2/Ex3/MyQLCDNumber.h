#include <QObject>
#include <QString>
#include <QLCDNumber>

class MyQLCDNumber: public QLCDNumber
{
   Q_OBJECT

   public:
     MyQLCDNumber (QWidget *parent);

   public slots:
     void reset ();
     void rebreop1(int numero);
     void rebreop2(int numero);
     void suma();
     void resta();
     void multiplica();
     void divideix();
     void absolut_activat();
     void acumula();

   signals:


   private:
     int op1, op2, resultat;
     bool absolut;

};
