#include "MyGLWidget.h"

#include <iostream>

MyGLWidget::MyGLWidget (QWidget* parent) : QOpenGLWidget(parent), program(NULL)
{
  setFocusPolicy(Qt::StrongFocus);  // per rebre events de teclat
  scale = 1.0f;
}

MyGLWidget::~MyGLWidget ()
{
  if (program != NULL)
    delete program;
}

void MyGLWidget::initializeGL ()
{
  // Cal inicialitzar l'ús de les funcions d'OpenGL
  initializeOpenGLFunctions();
  angle = 0.0;
  glClearColor(0.5, 0.7, 1.0, 1.0); // defineix color de fons (d'esborrat)
  glEnable(GL_DEPTH_TEST);
  carregaShaders();
  createBuffers();
  iniCamera();
}

void MyGLWidget::paintGL ()
{
  glViewport (0, 0, width(), height()); // Aquesta crida no caldria perquè Qt la fa de forma automàtica amb aquests paràmetres

  // Esborrem el frame-buffer
  glClear (GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  // Carreguem la transformació de model
  projectTransform();
  viewTransform();
  modelTransform_Homer();

  // Activem el VAO per a pintar la caseta
  glBindVertexArray (VAO_Homer);

  // pintem
  glDrawArrays(GL_TRIANGLES, 0, homer.faces().size()*3);

  glBindVertexArray (0);
  modelTransform_Terra();
  // Activem el VAO per a pintar la caseta
  glBindVertexArray (VAO_Terra);
  // pintem
  glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray (0);
}

void MyGLWidget::modelTransform_Homer()
{
  // Matriu de transformació de model
  glm::mat4 transform (1.0f);
  transform = glm::rotate(transform, angle, glm::vec3(0.0, 1.0, 0.0));
  transform = glm::scale(transform, glm::vec3(scale));
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &transform[0][0]);
}
void MyGLWidget::modelTransform_Terra()
{
  // Matriu de transformació de model
  glm::mat4 transform (1.0f);
  transform = glm::scale(transform, glm::vec3(scale));
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &transform[0][0]);
}

void MyGLWidget::projectTransform(){
  // glm::perspective(FOV en radians, ra window, znear, zfar)
  glm::mat4 Proj = glm::perspective(fov,ra,znear,zfar);
  glUniformMatrix4fv(projLoc,1,GL_FALSE,&Proj[0][0]);
}

void MyGLWidget::viewTransform(){
  //glm::lookAt(OBS, VRP, UP)
  glm::mat4 View = glm::lookAt(obs,vrp,up);
  glUniformMatrix4fv (viewLoc,1,GL_FALSE,&View[0][0]);
}


void MyGLWidget::resizeGL (int w, int h)
{
    float rViewport = float (w) / float (h);
    ra  = rViewport;
    if (rViewport < 1.0) {
        fov = 2.0 * atan(tan(fovi/2.0)/rViewport);
    }

    // Es conserva la vista. Mesures en pixels.
    glViewport(0, 0, w, h);
}


void MyGLWidget::keyPressEvent(QKeyEvent* event)
{
  makeCurrent();
  switch (event->key()) {
    case Qt::Key_S: { // escalar a més gran
      scale += 0.05;
      break;
    }
    case Qt::Key_D: { // escalar a més petit
      scale -= 0.05;
      break;
    }
    case Qt::Key_R: {
      angle += M_PI/4;
      break;
    }
    default: event->ignore(); break;
  }
  update();
}

void MyGLWidget::createBuffers ()
{
  //HOMER
  homer.load("/home/pol/Documents/Repos/IDI_Lab/models/HomerProves.obj");
  glGenVertexArrays(1, &VAO_Homer);
  glBindVertexArray(VAO_Homer);

  glGenBuffers(1, &VBO_HomerVert);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_HomerVert);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*homer.faces().size()*3*3, homer.VBO_vertices(), GL_STATIC_DRAW);

   // Activem l'atribut vertexLoc
   glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
   glEnableVertexAttribArray(vertexLoc);

   glGenBuffers(1, &VBO_HomerMat);
   glBindBuffer(GL_ARRAY_BUFFER, VBO_HomerMat);
   glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*homer.faces().size()*3*3, homer.VBO_matdiff(), GL_STATIC_DRAW);

   // Activem l'atribut colorLoc
   glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
   glEnableVertexAttribArray(colorLoc);

   // TERRA
 glm::vec3 Vertices[6];  // 6 vèrtexs amb X, Y i Z
 Vertices[0] = glm::vec3(-1.0, -1.0, 1.0);
 Vertices[1] = glm::vec3(1.0, -1.0, 1.0);
 Vertices[2] = glm::vec3(1.0, -1.0, -1.0);
 Vertices[3] = glm::vec3(-1.0, -1.0, 1.0);
 Vertices[4] = glm::vec3(-1.0, -1.0, -1.0);
 Vertices[5] = glm::vec3(1.0, -1.0, -1.0);

 glm::vec3 Colors[6];   // color del terra
 Colors[0] = glm::vec3(1.0, 0.0, 0.0);
   Colors[1] = glm::vec3(1.0, 0.0, 0.0);
   Colors[2] = glm::vec3(1.0, 0.0, 0.0);
   Colors[3] = glm::vec3(1.0, 0.0, 0.0);
   Colors[4] = glm::vec3(1.0, 0.0, 0.0);
   Colors[5] = glm::vec3(1.0, 0.0, 0.0);
 // Creació del Vertex Array Object (VAO) que usarem per pintar
 glGenVertexArrays(1, &VAO_Terra);
 glBindVertexArray(VAO_Terra);

 // Creació del buffer amb les dades dels vèrtexs
 glGenBuffers(1, &VBO_TerraVert);
 glBindBuffer(GL_ARRAY_BUFFER, VBO_TerraVert);
 glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
 // Activem l'atribut que farem servir per vèrtex
 glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
 glEnableVertexAttribArray(vertexLoc);
   // Creació del buffer amb les dades dels vèrtexs
   glGenBuffers(1, &VBO_TerraColor);
   glBindBuffer(GL_ARRAY_BUFFER, VBO_TerraColor);
   glBufferData(GL_ARRAY_BUFFER, sizeof(Colors), Colors, GL_STATIC_DRAW);
   // Activem l'atribut que farem servir per vèrtex
   glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
   glEnableVertexAttribArray(colorLoc);
 // Desactivem el VAO
 glBindVertexArray(0);
}

void MyGLWidget::carregaShaders()
{
  // Creem els shaders per al fragment shader i el vertex shader
  QOpenGLShader fs (QOpenGLShader::Fragment, this);
  QOpenGLShader vs (QOpenGLShader::Vertex, this);
  // Carreguem el codi dels fitxers i els compilem
  fs.compileSourceFile("shaders/fragshad.frag");
  vs.compileSourceFile("shaders/vertshad.vert");
  // Creem el program
  program = new QOpenGLShaderProgram(this);
  // Li afegim els shaders corresponents
  program->addShader(&fs);
  program->addShader(&vs);
  // Linkem el program
  program->link();
  // Indiquem que aquest és el program que volem usar
  program->bind();

  // Obtenim identificador per a l'atribut “vertex” del vertex shader
  vertexLoc = glGetAttribLocation (program->programId(), "vertex");
  // Obtenim identificador per a l'atribut “color” del vertex shader
  colorLoc = glGetAttribLocation (program->programId(), "color");
  // Uniform locations
  transLoc = glGetUniformLocation(program->programId(), "TG");
  projLoc = glGetUniformLocation(program->programId(), "proj");
  viewLoc = glGetUniformLocation(program->programId(), "view");
}


void MyGLWidget::iniCamera()
{
    radiEsferaContenidora();

    float d = 2 * radi;   // distància de l'OBS al VRP
    v   = glm::vec3(0.0, 0.0, 1.0);  // v = vector normalitzat en qualsevol direcció

    vrp = glm::vec3(0.0, 0.0, 0.0);
    obs = vrp;   // OBS = VRP + d * v, on d > radi
    for (int i = 0; i < 3; i += 1){
        obs[i] += v[i] * d;
    }
    up = glm::vec3(0.0, 1.0, 0.0);

    znear = d - radi;
    zfar  = d + radi;
    fovi  = 2.0 * asin(radi / d); // (float)M_PI / 2.0f;
    fov   = fovi;
    ra    = 1.0;
    angle = 0.0;

    viewTransform ();
    projectTransform ();
}

void MyGLWidget::radiEsferaContenidora()
{
    float xmin = -1.0;
    float ymin = -1.0;
    float zmin = -1.0;
    float xmax =  1.0;
    float ymax =  1.0;
    float zmax =  1.0;
    float dx   = xmax - xmin;
    float dy   = ymax - ymin;
    float dz   = zmax - zmin;
    radi       = sqrt(dx * dx + dy * dy + dz * dz)/2.0;
    centre[0]  = (xmax + xmin) / 2.0;
    centre[1]  = (ymax + ymin) / 2.0;
    centre[2]  = (zmax + zmin) / 2.0;
}
