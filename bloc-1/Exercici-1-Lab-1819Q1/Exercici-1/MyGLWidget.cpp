#include "MyGLWidget.h"

#include <iostream>

MyGLWidget::MyGLWidget (QWidget* parent) : QOpenGLWidget(parent), program(NULL)
{
  setFocusPolicy(Qt::StrongFocus);  // per rebre events de teclat
}

MyGLWidget::~MyGLWidget ()
{
  if (program != NULL)
    delete program;
}

void MyGLWidget::initializeGL ()
{
  // Cal inicialitzar l'ús de les funcions d'OpenGL
  stat = true;
  ex = ey = 1.0;
  tx = ty = 0.0;
  angle = 0.0;
  initializeOpenGLFunctions();
  glClearColor (0.5, 0.7, 1.0, 1.0); // defineix color de fons (d'esborrat)
  carregaShaders();
  createBuffersQuadrat();
}

void MyGLWidget::pintaQuadrat ()  // AQUEST MÈTODE NO ES POT MODIFICAR !!!
{
  // Activem l'Array a pintar
  glBindVertexArray(VAO1);
  // Pintem el quadrat
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void MyGLWidget::paintGL ()
{
  if(stat){
    glViewport (0, 0, width(), height()); // Aquesta crida no caldria perquè Qt la fa de forma automàtica amb aquests paràmetres
    glClear (GL_COLOR_BUFFER_BIT);  // Esborrem el frame-buffer
    // Aquí caldrà pintar l'escena de la forma adient...
    angle = 0.0;
    ex = 1.5;
    ey = 0.5;
    tx = -0.3;
    ty = -0.5;
    modelTransformR2();
    pintaQuadrat();
    glBindVertexArray(0);
    ex = 0.5;
    ey = 2.5;
    tx = -0.5;
    ty = 0.1;
    modelTransformR1();
    pintaQuadrat ();
    // Desactivem el VAO
    glBindVertexArray(0);
  }
  else{
    glViewport (0, 354., width()/2, height()/2); // Aquesta crida no caldria perquè Qt la fa de forma automàtica amb aquests paràmetres
    glClear (GL_COLOR_BUFFER_BIT);  // Esborrem el frame-buffer
    // Aquí caldrà pintar l'escena de la forma adient...
    angle = 0.0;
    ex = 1.5;
    ey = 0.5;
    tx = -0.3;
    ty = -0.5;
    modelTransformR2();
    pintaQuadrat();
    glBindVertexArray(0);
    ex = 0.5;
    ey = 2.5;
    tx = -0.5;
    ty = 0.1;
    modelTransformR1();
    pintaQuadrat ();
    glBindVertexArray(0);
    // Desactivem el VAO
    glViewport (354., 0, width()/2, height()/2); // Aquesta crida no caldria perquè Qt la fa de forma automàtica amb aquests paràmetres
    // Aquí caldrà pintar l'escena de la forma adient...
    angle = M_PI;
    ex = 1.5;
    ey = 0.5;
    tx = 0.3;
    ty = 0.5;
    modelTransformR2();
    pintaQuadrat();
    glBindVertexArray(0);
    ex = 0.5;
    ey = 2.5;
    tx = 0.5;
    ty = -0.1;
    modelTransformR1();
    pintaQuadrat ();
    // Desactivem el VAO
    glBindVertexArray(0);
  }
}

void MyGLWidget::modelTransformR1 ()
{
  glm::mat4 TG (1.0); // Matriu de transformació, inicialment identitat
  TG = glm::translate(TG, glm::vec3 (tx, ty, 0));
  TG = glm::scale(TG, glm::vec3 (ex, ey, 1));
  TG = glm::rotate(TG, angle, glm::vec3(0.0, 0.0, 1.0));
  glUniformMatrix4fv (transLoc, 1, GL_FALSE, &TG[0][0]);
}

void MyGLWidget::modelTransformR2 ()
{
  glm::mat4 TG (1.0); // Matriu de transformació, inicialment identitat
  TG = glm::translate(TG, glm::vec3 (tx, ty, 0));
  TG = glm::scale(TG, glm::vec3 (ex, ey, 1));
  TG = glm::rotate(TG, angle, glm::vec3(0.0, 0.0, 1.0));
  glUniformMatrix4fv (transLoc, 1, GL_FALSE, &TG[0][0]);
}

void MyGLWidget::resizeGL (int w, int h)
{
  // Aquí anirà el codi que cal fer quan es redimensiona la finestra
  // no es demana en aquest exercici!
}

void MyGLWidget::keyPressEvent(QKeyEvent* event)
{
  makeCurrent();
  switch (event->key()) {
    case Qt::Key_V: {
      stat = !stat; // canvi entre un i dos viewports
      break;
    }
    default: event->ignore(); break;
  }
  update();
}

void MyGLWidget::createBuffersQuadrat ()  // AQUEST MÈTODE NO ES POT MODIFICAR !!!
{
  glm::vec3 Vertices[4];  // Quatre vèrtexs amb X, Y i Z
  Vertices[0] = glm::vec3(-0.2, -0.2, 0.0);
  Vertices[1] = glm::vec3(0.2, -0.2, 0.0);
  Vertices[2] = glm::vec3(-0.2, 0.2, 0.0);
  Vertices[3] = glm::vec3(0.2, 0.2, 0.0);

  glm::vec3 Colors[4];
  Colors[0] = glm::vec3(1.0, 1.0, 0.0);
  Colors[1] = glm::vec3(1.0, 0.0, 0.0);
  Colors[2] = glm::vec3(1.0, 1.0, 0.0);
  Colors[3] = glm::vec3(1.0, 0.0, 0.0);

  // Creació del Vertex Array Object (VAO) que usarem per pintar
  glGenVertexArrays(1, &VAO1);
  glBindVertexArray(VAO1);

  // Creació del buffer amb les dades dels vèrtexs
  GLuint VBOs[2];
  glGenBuffers(2, VBOs);
  glBindBuffer(GL_ARRAY_BUFFER, VBOs[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
  // Activem l'atribut que farem servir per vèrtex (només el 0 en aquest cas)
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(vertexLoc);

  glBindBuffer(GL_ARRAY_BUFFER, VBOs[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Colors), Colors, GL_STATIC_DRAW);
  // Activem l'atribut que farem servir per vèrtex (només el 0 en aquest cas)
  glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(colorLoc);

  // Desactivem el VAO
  glBindVertexArray(0);
}

void MyGLWidget::carregaShaders()
{
  // Creem els shaders per al fragment shader i el vertex shader
  QOpenGLShader fs (QOpenGLShader::Fragment, this);
  QOpenGLShader vs (QOpenGLShader::Vertex, this);
  // Carreguem el codi dels fitxers i els compilem
  fs.compileSourceFile("shaders/fragshad.frag");
  vs.compileSourceFile("shaders/vertshad.vert");
  // Creem el program
  program = new QOpenGLShaderProgram(this);
  // Li afegim els shaders corresponents
  program->addShader(&fs);
  program->addShader(&vs);
  // Linkem el program
  program->link();
  // Indiquem que aquest és el program que volem usar
  program->bind();

  // Obtenim identificador per a l'atribut “vertex” del vertex shader
  vertexLoc = glGetAttribLocation (program->programId(), "vertex");
  // Obtenim identificador per a l'atribut “color” del vertex shader
  colorLoc = glGetAttribLocation (program->programId(), "color");
  transLoc = glGetUniformLocation (program->programId(), "TG");
}
